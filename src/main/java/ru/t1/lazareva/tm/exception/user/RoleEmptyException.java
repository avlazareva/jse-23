package ru.t1.lazareva.tm.exception.user;

import ru.t1.lazareva.tm.exception.user.AbstractUserException;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}